/**
 * Created by ������ on 09.07.2015.
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;


public class NotePadFrame extends JFrame implements  MouseListener
{
    private Container contentPane;
    private JPanel mainPanel;
    private JTextArea textArea;
    JMenuBar menuBar;
    JMenu menu;
    JMenu saveAsMenu;
    JFileChooser fileChooser;

    public NotePadFrame(String title)
    {
        super(title);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        contentPane = this.getContentPane();
        createGUI();
    }

    public void createGUI()
    {
        mainPanel = new JPanel();
        mainPanel.setLayout(null);
        contentPane.add(mainPanel);
        mainPanel.setLayout(new BorderLayout());
        textArea = new JTextArea();
        JScrollPane scrollPane = new JScrollPane(textArea);
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        textArea.requestFocusInWindow();
        mainPanel.add(scrollPane);
        fileChooser = new JFileChooser();

        this.setJMenuBar(createMenu());
    }

    public JMenuBar createMenu()
    {
        menuBar = new JMenuBar();
        menu = new JMenu("Menu");
        menuBar.add(menu);

        saveAsMenu = new JMenu("Save As");
        saveAsMenu.addMouseListener(this);
        menu.add(saveAsMenu);

        return menuBar;
    }


    @Override
    public void mouseClicked(MouseEvent e)
    {
        //
    }

    @Override
    public void mousePressed(MouseEvent e)
    {
        if(!textArea.getText().equals(""))
        {
            JMenu menu = (JMenu) e.getSource();
            if(menu.getText().equals(saveAsMenu.getText()))
            {
                fileChooser.showSaveDialog(NotePadFrame.this);
                File file = fileChooser.getSelectedFile();
                String path = file.getAbsolutePath();
                String fileName = file.getName() + ".txt";

                try {
                    PrintWriter writer = new PrintWriter(fileName, "UTF-8");
                    writer.println(textArea.getText());
                    writer.close();
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                } catch (UnsupportedEncodingException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e)
    {
        //
    }

    @Override
    public void mouseEntered(MouseEvent e)
    {
        //
    }

    @Override
    public void mouseExited(MouseEvent e)
    {
        //
    }
}